TEAM TPZ

NOTE: MPDP-CA2-BaseGameFileDirectory.XLSX defines each source file in base game specifying what file does and whether or not we need to change said file. Please refer to this if you're lost or unsure where to go/what to do.

Multiplayer CA2

Game Concept

BulletMaze (totally up for other names)

Top down 2d Action Shooter

Up to 4 special operations troopers have to navigate maze like stacks of crates in a giant warehouse or cargo ship hold. The mission, get the nuke in the middle of the maze and/or kill your opponents. You can use guns and grenades to complete your mission. Use crates as cover, but watch out as some can be destroyed. Pickup ammo and grenades as you move.

Nice to haves: Different gun types (shotgun, pistol, assault rifle), AI enemies (either enemy troops, turrets, mines, or booby traps). Destructible crates are a nice to have, but game will be pretty lame without it.

What needs to change: Base game scrolls up in the Y with a narrow (screen width in the X), long (in the Y) background texture. We will change this to a large square-ish image. Players will spawn at predetermined coordinates in the four corners, and have a predefined viewport, so they'll see a small rectangular portion of large background. The map will have stacks of crates arranged vertically and horizontally creating a maze. There will be some kind of goal in the center. Player movement will be horizontal (x) and vertical (y) only. Player sprite will rotate based on direction of travel and fire bullets from their front (direction they are pointed.). When the player gets within x pixels of a viewport border, their view will shift in that direction. Winning happens when player kills all opponents or gets to goal at center. 

Speed -> nice to have - allow sprinting for short time requiring a recharge before re-use

Grenades - like base game a player will get N number of grenades. Grenades will travel in straight line forward and detonate after M seconds, damaging all in a blast radius of O.

There will be health and ammo pickups. As an alternative, we can look into weapon upgrades or stuff like deployable traps, turrets, or mines to make the game more interesting. Body Armor is possible.

What needs to change:
{World: new background image, default travel speed, player viewport, wold movement, create map, add crates and collisions
Change Aircraft to Trooper (mostly done)
Replace Missiles with Grenades, remove guided missile, replace with grenade behavior
Player - change assets (have them), animate them (moving, shooting, toss grenade, standing idle, deploying trap etc, dying...)
Collisions - player vs player (bounce off each other with no damage, ++ would be melee combat), bullets, grenade damage, and crate collision (can't clip through walls)
Add goal mechanic (like a Nuke) and art assets. Pickup the nuke and win
Destructible crates - crates with hit points that get removed when HP <=0}

Other stuff:
{Menus for game types that work, credits, options, etc.
New title image
Better fonts throughout
Win Screen 
Lose Screen}

Nice to have 
{AI enemies or turrets
Pre-deployed traps/mines
Coop and PVP multiplayer
Speed changes for weapon/armor (more you carry, more youre slowed)
Cool particle effects - grenade or trap detonation, shotgun blasts, maybe melee (bloodsplatter)
Shaders - we need to get some shaders in to make up for last game JL stressed lack of shader work in feedback}

These are basics, let me know if you have questions, suggestions or other feedback.

PC




