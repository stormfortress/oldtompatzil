#pragma once
#ifndef BOOK_CATEGORY_HPP
#define BOOK_CATEGORY_HPP


// Entity/scene node category, used to dispatch commands
namespace Category
{
	enum Type
	{
		None = 0,
		SceneAirLayer = 1 << 0,
		PlayerTrooper = 1 << 1,
		AlliedTrooper = 1 << 2,
		EnemyTrooper = 1 << 3,
		Pickup = 1 << 4,
		AlliedProjectile = 1 << 5,
		EnemyProjectile = 1 << 6,
		ParticleSystem = 1 << 7,
		SoundEffect = 1 << 8,
		Network = 1 << 9,

		Trooper = PlayerTrooper | AlliedTrooper | EnemyTrooper,
		Projectile = AlliedProjectile | EnemyProjectile,
	};
}

#endif // BOOK_CATEGORY_HPP
