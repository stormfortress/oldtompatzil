#pragma once
#include "Entity.hpp"
#include "Command.hpp"
#include "ResourceIdentifiers.hpp"
#include "Projectile.hpp"
#include "TextNode.hpp"
#include "Animation.hpp"

#include <SFML/Graphics/Sprite.hpp>

class Trooper : public Entity
{
public:
	enum Type
	{
		Thug,
		Merc,
		Gangsta,
		SOG,
		TypeCount
	};
public:
	Trooper(Type type, const TextureHolder& textures, const FontHolder& fonts);

	virtual unsigned int	getCategory() const;
	virtual sf::FloatRect	getBoundingRect() const;
	virtual void			remove();
	virtual bool			isMarkedForRemoval() const;
	bool					isAllied() const;
	float					getMaxSpeed() const;//might not need??
	void					disablePickups() const;
	void					increaseFireRate(); //probably can ditch later
	void					increaseSpread(); //also ditch
	void					collectGrenades(unsigned int count);
	void					fire();
	void					deployGrenade();
	void					playLocalSound(CommandQueue& commands, SoundEffect::ID effect);
	int						getIdentifier();
	void					setIdentifier(int identifier);
	int						getGrenades() const;
	void					setGrenades(int ammo);

private:
	virtual void			drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void			updateCurrent(sf::Time dt, CommandQueue& commands);
	void					updateMovementPattern(sf::Time dt);
	void					checkPickupDrop(CommandQueue& commands);
	void					checkDeployGrenade(sf::Time dt, CommandQueue& commands);
	void					createBullets(SceneNode& node, const TextureHolder& textures);
	//void					createGrenade(SceneNode& node, Grenade);//left off here, renamed missile to grenades
	void					createPickup(SceneNode& node, const TextureHolder& textures) const;
	void					updateTexts();
	void					updateRollAnimation();//nix or change this - our game should have many more animations than base game
		
private:
	Type					mType;
	sf::Sprite				mSprite;
	Animation				mExplosion;
	Command					mFireCommand;
	Command					mGrenadeCommand;
	sf::Time				mFireCountdown;
	bool					mIsFiring;
	bool					mIsDeployingGrenade;
	bool					mShowExplosion;
	bool					mExplosionBegan;
	bool					mPlayedExplosionSound;
	bool					mSpawnedPickup;
	bool					mPickupsEnabled;
	int						mFireRateLevel;
	int						mSpreadLevel;
	int						mGrenadeAmmo;
	Command					mDropPickupCommand;
	float					mTravelledDistance;
	std::size_t				mDirectionIndex;
	TextNode*				mHealthDisplay;
	TextNode*				mGrenadeDisplay;
	int						mIdentifier;
		//but might keep 'projectile' to keep things simple...
};